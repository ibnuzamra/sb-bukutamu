package com.artivisi.training.devops201903.springbootbukutamu.dao;

import com.artivisi.training.devops201903.springbootbukutamu.entity.Bukutamu;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BukutamuDao extends PagingAndSortingRepository<Bukutamu, String> {
}
